from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from keras import models
from keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Neural Network Architecture
cnn = models.Sequential()
cnn.add(Conv2D(32, (3,3), activation='relu', input_shape=(28,28,1)))
cnn.add(MaxPooling2D((2,2)))
cnn.add(Conv2D(64,(3,3), activation='relu'))
cnn.add(MaxPooling2D((2,2)))
cnn.add(Conv2D(64, (3,3), activation='relu'))
cnn.add(Flatten())
cnn.add(Dense(64, activation='relu'))
cnn.add(Dense(10, activation='softmax'))

# Dataset load

(img_trn, labels_trn), (img_tst, labels_tst) = mnist.load_data()

# Image pre-processing

img_trn = img_trn.reshape((60000, 28, 28, 1))
img_trn = img_trn.astype('float32')/255

img_tst = img_tst.reshape((10000, 28, 28, 1))
img_tst = img_tst.astype('float32')/255

labels_trn, labels_tst = to_categorical(labels_trn), to_categorical(labels_tst)

# Model Compiling and training


cnn.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
cnn.fit(img_trn, labels_trn, epochs=1, batch_size=128)


# Model Evaluation

test_loss, test_accuracy = cnn.evaluate(img_tst, labels_tst)
print(f'test accuracy is: {test_accuracy}')
